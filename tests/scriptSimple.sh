#!/bin/bash

APP_URL=$1
APP_PORT=$2

echo "suppression des fichiers log genere par le script"
rm count.*
rm ping.*
echo "verification de l'existance d'un serveur sur le port $APP_PORT :"
echo 
netstat -peanut | grep ${APP_PORT}
echo . 
echo "verification de l'existance de la page ping: "
curl ${APP_URL}:${APP_PORT}/ping
echo
echo
echo "verification de l'existance de la page count: "
curl ${APP_URL}:${APP_PORT}/count
echo 
echo
echo "---------------------------------------------------------------------------------"
echo "Utilisation de WGET !"
echo "Nous allons dans un premiers temps verifier le nombre de ping deja envoye :"
wget ${APP_URL}:${APP_PORT}/count -q
resultat1=$(cat count.1)
echo 
echo "nous avons actuellement " $resultat1 "ping envoye "
echo
echo "envoie d'un ping pour verifier la reception d'un pong mais aussi l'incrementation de count "
wget ${APP_URL}:${APP_PORT}/ping -q
resultat2=$(cat ping.1)
echo $resultat2
wget ${APP_URL}:${APP_PORT}/count -q
resultat3=$(cat count.2)
echo "nous avons actuellement " $resultat3 "ping envoye a la page"
echo "---------------------------------------------------------------------------------"
echo
echo
echo "---------------------------------------------------------------------------------"
echo "utilisation de curl"
echo "affichage du nombre de ping envoye"
curl ${APP_URL}:${APP_PORT}/count
echo
echo " envoie d'un ping "
curl ${APP_URL}:${APP_PORT}/ping
echo
echo "nombre de ping envoye +1"
curl ${APP_URL}:${APP_PORT}/count
echo
echo "---------------------------------------------------------------------------------"
echo
echo
echo "---------------------------------------------------------------------------------"
echo "utilisation de JQ "
echo
echo " affichage du nombre de ping"
curl ${APP_URL}:${APP_PORT}/count | jq -R
echo
echo
echo "envoie d'un ping avec resultat"
curl ${APP_URL}:${APP_PORT}/ping | jq -R
curl ${APP_URL}:${APP_PORT}/count | jq -R
echo "---------------------------------------------------------------------------------"










