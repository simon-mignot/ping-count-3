#!/bin/bash
echo "suppression des fichiers log genere par le script"
rm count.*
rm ping.*

read -p "Veuillez rentrer le port de votre serveur :" PortNumber
echo "verification de l'existance d'un serveur sur le port "$PortNumber " :"
echo 
netstat -peanut | grep $PortNumber
echo 
read -p "entrer l'adresse ip de votre serveur : " IpServeur
echo "tentative de ping sur votre serveur"
ping $IpServeur -c 3 

echo "verification de l'existance de la page ping sur votre serveur: "
curl $IpServeur:$PortNumber/ping
echo
echo
echo "verification de l'existance de la page count: "
curl $IpServeur:$PortNumber/count
echo 
echo
echo "---------------------------------------------------------------------------------"
echo "Utilisation de WGET !"
echo "Nous allons dans un premiers temps verifier le nombre de ping deja envoye :"
wget $IpServeur:$PortNumber/count -q 
resultat1=$(cat count.1)
echo 
echo "nous avons actuellement " $resultat1 "ping envoye "
echo
echo "envoie d'un ping pour verifier la reception d'un pong mais aussi l'incrementation de count "
wget $IpServeur:$PortNumber/ping -q
resultat2=$(cat ping.1)
echo $resultat2
wget $IpServeur:$PortNumber/count -q 
resultat3=$(cat count.2)
echo "nous avons actuellement " $resultat3 "ping envoye a la page"
echo "---------------------------------------------------------------------------------"
echo
echo
echo "---------------------------------------------------------------------------------"
echo "utilisation de curl"
echo "affichage du nombre de ping envoye"
curl $IpServeur:$PortNumber/count
echo
echo " envoie d'un ping "
curl $IpServeur:$PortNumber/ping
echo
echo "nombre de ping envoye +1"
curl $IpServeur:$PortNumber/count
echo
echo "---------------------------------------------------------------------------------"
echo
echo
echo "---------------------------------------------------------------------------------"
echo "utilisation de JQ "
echo
echo " affichage du nombre de ping"
curl -s $IpServeur:$PortNumber/count | jq ".pingCount"
echo
echo
echo "envoie d'un ping avec resultat"
curl -s $IpServeur:$PortNumber/ping | jq  ".message"
curl -s $IpServeur:$PortNumber/count | jq ".pingCount"
echo "---------------------------------------------------------------------------------"

