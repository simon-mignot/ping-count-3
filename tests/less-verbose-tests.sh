#!/bin/bash

# Check response format
if [[ !($(curl ${APP_URL}:${APP_PORT}/ping 2> /dev/null | jq -er 'has("message")') == true \
    && $(curl ${APP_URL}:${APP_PORT}/ping 2> /dev/null | jq -r '.message') == 'pong') ]]
then
    echo "$APP_URL: bad /pong request format"
    exit 1
fi

if [[ !($(curl ${APP_URL}:${APP_PORT}/count 2> /dev/null | jq -er 'has("pingCount")')) ]]
then
    echo "$APP_URL: bad /count request format"
    exit 2
fi

# Check incrementation
# Initial count
count=$(curl ${APP_URL}:${APP_PORT}/count 2> /dev/null | jq -r ".pingCount")
for i in {1..10}
do
    res=$(curl ${APP_URL}:${APP_PORT}/ping 2> /dev/null)
    res=$(curl ${APP_URL}:${APP_PORT}/count 2> /dev/null | jq -r ".pingCount")
    ((count++))
done
if [[ "$count" -eq "$res" ]]
then
    echo "$APP_URL: OK"
else
    echo "$APP_URL: Bad count incrementation"
    exit 3
fi


