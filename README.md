# PingCount

Bibliothèques utilisées :

* [[github] Pistache](https://github.com/oktal/pistache)
* [[github] restclient-cpp](https://github.com/mrtazz/restclient-cpp)

Installation
```
# Arborescence utilisée :
# BASE_DIR/pingcount-install-tmp # Compilation et installation des bibliothèques tierces
# BASE_DIR/src                   # Compilation et installation de PingCount
# BASE_DIR/src/install.sh        # Script d'installation, à exectuer à partir de BASE_DIR
# BASE_DIR/PingCount             # Chemin de sortie de l'executable après le script d'installation, à executer à partir de BASE_DIR
# BASE_DIR/{private|public}.dsn  # Fichier contenant la clé dsn

# Installation des dépendances puis de PingCount dans ./pingcount/PingCount
mkdir pingcount && cd pingcount
git clone https://gitlab.com/simon-mignot-epsi/ping-count-3.git ./ping-count-3
./ping-count-3/install.sh

# Configuration du dsn
echo {private-dsn} > ./private.dsn
#OU
#echo {public-dsn} > ./public.dsn

# Lancement du serveur
./PingCount
```