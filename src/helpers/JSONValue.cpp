//
// Created by Simon Mignot on 01/12/17.
//

#include "JSONValue.h"

template <>
std::string JSONValue<std::string>::toJSON() const
{
    return '"' + m_value + '"';
}

template <>
std::string JSONValue<bool>::toJSON() const
{
    return m_value ? "true" : "false";
}
