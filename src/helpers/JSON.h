//
// Created by Simon Mignot on 01/12/17.
//

#ifndef PINGCOUNT_JSON_H
#define PINGCOUNT_JSON_H

#include <iostream>
#include <map>
#include <vector>

class JSONAbstract
{
    public:
        virtual std::string toJSON() const = 0;
        friend std::ostream& operator<<(std::ostream& o, JSONAbstract const& jsonAbstract);
    private:
};

class JSONObject : public JSONAbstract, public std::map<std::string, JSONAbstract*>
{
    public:
        friend std::ostream& operator<<(std::ostream& o, JSONObject const& jsonObject);
        std::string toJSON() const;
    private:
};

class JSONArray : public JSONAbstract, public std::vector<JSONAbstract*>
{
    public:
        friend std::ostream& operator<<(std::ostream& o, JSONArray const& jsonArray);
        std::string toJSON() const;
    private:
};


#endif //PINGCOUNT_JSON_H
