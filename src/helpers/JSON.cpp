//
// Created by Simon Mignot on 01/12/17.
//

#include "JSON.h"

std::ostream& operator<<(std::ostream& o, JSONAbstract const& jsonAbstract)
{
    o << jsonAbstract.toJSON();
    return o;
}

std::string JSONObject::toJSON() const
{
    std::string result;
    result += '{';

    for(std::map<std::string, JSONAbstract*>::const_iterator it = begin(); it != end(); ++it)
    {
        // TODO: Use JSONValue<std::string> for the key (and overload operator+)
        result += '"' + it->first + '"' + ':' + it->second->toJSON();
        if(std::distance(it, end()) > 1)
            result += ',';
    }
    result += '}';
    return result;
}

std::ostream& operator<<(std::ostream& o, JSONObject const& jsonObject)
{
    o << jsonObject.toJSON();
    return o;
}

std::string JSONArray::toJSON() const
{
    std::string result;
    result += '[';

    for(std::vector<JSONAbstract*>::const_iterator it = begin(); it != end(); ++it)
    {
        // TODO: Use JSONValue<std::string> for the key (and overload operator+)
        result += (*it)->toJSON();
        if(std::distance(it, end()) > 1)
            result += ',';
    }
    result += ']';
    return result;
}

std::ostream& operator<<(std::ostream& o, JSONArray const& jsonArray)
{
    o << jsonArray.toJSON();
    return o;
}