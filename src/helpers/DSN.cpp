//
// Created by Simon Mignot on 01/12/17.
//

#include "DSN.h"
#include "SentryClient.h"

// ======================
// MalformedDSNException
// ======================
MalformedDSNException::MalformedDSNException() : m_message("Malformed DSN.")
{

}
MalformedDSNException::MalformedDSNException(std::string message) : m_message("Malformed DSN : " + message)
{

}
std::string MalformedDSNException::what()
{
    return m_message;
}


// ====
// DSN
// ====
DSN::DSN(const std::string& dsn) : m_sentryVersion(SentryClient::getSentryVersion()), m_version(SentryClient::getSDKUserAgent())
{
    parseDSN(dsn);
}

void DSN::parseDSN(std::string dsn)
{
    try { m_protocol = popUntil(dsn, "://"); }
    catch(Exception& e) { throw MalformedDSNException(); }

    std::string keys;
    try { keys = popUntil(dsn, "@"); }
    catch(Exception& e) { throw MalformedDSNException(); }

    m_privateDSN = keys.find(':') != std::string::npos;
    try { m_publicKey = popUntil(keys, m_privateDSN ? ":" : ""); }
    catch(Exception& e) { throw MalformedDSNException(); }

    try { m_secretKey = popUntil(keys, ""); }
    catch(Exception& e) { }

    try
    {
        m_host =      popUntil(dsn, "/");
        m_projectID = popUntil(dsn, "");
    }
    catch(Exception& e) { throw MalformedDSNException(); }

    if(!verifyProtocol() || !verifyKeys())
        throw MalformedDSNException("Invalid key(s) format.");
}

std::string DSN::popUntil(std::string& dsn, const std::string& sep)
{
    if(sep.empty())
        return dsn;
    size_t pos = dsn.find(sep);
    if(pos == std::string::npos)
        throw Exception::SEPARATOR_NOT_FOUND;
    std::string value = dsn.substr(0, pos);
    dsn = dsn.substr(pos + sep.size());
    return value;
}

bool DSN::verifyKeys()
{
    std::string hexChars = "0123456789abcdefABCDEF";
    return (m_publicKey.size() == 32 && (m_secretKey.size() == 32 || !m_privateDSN))
           &&
           !((m_secretKey.find_first_not_of(hexChars) != std::string::npos && m_privateDSN)
             || m_publicKey.find_first_not_of(hexChars) != std::string::npos);
}
bool DSN::verifyProtocol()
{
    return m_protocol == "http" || m_protocol == "https";
}

std::string DSN::getReferer()
{
    return m_protocol + "://" + m_host;
}

std::string DSN::getProjectPath()
{
    return "/api/" + m_projectID + "/store/";
}

std::string DSN::getXSentryAuth(time_t timestamp)
{
    std::string sentrySecret;
    if(m_privateDSN)
        sentrySecret = "sentry_secret=" + m_secretKey + ',';
    return "Sentry sentry_version=" + m_sentryVersion + ','
           + "sentry_timestamp=" + std::to_string(timestamp) + ','
           + "sentry_key=" + m_publicKey + ','
           + sentrySecret
           + "sentry_client=restclient-cpp/1.0";
}




