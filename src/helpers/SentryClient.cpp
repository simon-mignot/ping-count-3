//
// Created by Simon Mignot on 30/11/17.
//

#include <restclient-cpp/restclient.h>
#include "SentryClient.h"

JSONObject* SentryClient::m_sdkInfos = nullptr;
SentryClient* SentryClient::m_instance = nullptr;
bool SentryClient::m_isInitialized = false;

bool SentryClient::configureClient(std::string dsn_str)
{
    try
    {
        if(!dsn_str.empty() && !m_isInitialized)
            init();
        if(m_instance == nullptr)
            m_instance = new SentryClient(new DSN(dsn_str));
        return true;
    }
    catch(MalformedDSNException& e)
    {
        std::cout << e.what() << '\n';
        delete m_instance;
        m_instance = nullptr;
        return false;
    }
}

SentryClient* SentryClient::getInstance()
{
    return m_instance;
}


SentryClient::SentryClient(DSN* dsn) : m_dsn(dsn)
{
    m_connection = new RestClient::Connection(dsn->getReferer());
    RestClient::HeaderFields headerFields;
    headerFields["Content-Type"] = "application/json";
    headerFields["User-Agent"] = getSDKUserAgent();
    m_connection->SetHeaders(headerFields);
}

void SentryClient::displayDSNInfos() const
{
    std::cout << "Referer: " << m_dsn->getReferer() << '\n';
    std::cout << "Project path: " << m_dsn->getProjectPath() << '\n';
    std::cout << "X-Sentry-Auth: " << m_dsn->getXSentryAuth() << '\n';
}

JSONObject* SentryClient::getSdkInfos()
{
    if(m_sdkInfos == nullptr)
    {
        m_sdkInfos = new JSONObject();
        (*m_sdkInfos)["name"] = new JSONValue<std::string>("restclient-cpp");
        (*m_sdkInfos)["version"] = new JSONValue<std::string>("1.0");
    }
    return m_sdkInfos;
}

std::string SentryClient::getSDKUserAgent()
{
    std::string name = dynamic_cast<JSONValue<std::string>*>((*getSdkInfos())["name"])->get();
    std::string version = dynamic_cast<JSONValue<std::string>*>((*getSdkInfos())["version"])->get();
    return name + '/' + version;
}

std::string SentryClient::getSentryVersion()
{
    return "7";
}

void SentryClient::init()
{
    RestClient::init();
    m_isInitialized = true;
}

void SentryClient::sendEvent(SentryEvent& event)
{
    time_t tm = std::time(nullptr);
    RestClient::HeaderFields headerFields = m_connection->GetHeaders();

    headerFields["X-Sentry-Auth"] = m_dsn->getXSentryAuth(tm);
    event.setTimestamp(tm);

    m_connection->SetHeaders(headerFields);

    RestClient::Response response = m_connection->post(m_dsn->getProjectPath(), event.getJSONBody());
}

