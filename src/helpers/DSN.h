//
// Created by Simon Mignot on 01/12/17.
//

#ifndef PINGCOUNT_DSN_H
#define PINGCOUNT_DSN_H

#include <iostream>
#include <ctime>

class MalformedDSNException : std::exception
{
    public:
        MalformedDSNException();
        MalformedDSNException(std::string message);

        std::string what();

    private:
        std::string m_message;
};

class DSN
{
    public:
        explicit DSN(const std::string& dsn);

        std::string getReferer();
        std::string getProjectPath();
        std::string getXSentryAuth(time_t timestamp = std::time(nullptr));

    private:
        // Methods
        bool verifyKeys();
        bool verifyProtocol();
        void parseDSN(std::string dsn);
        std::string popUntil(std::string& dsn, const std::string& sep);

        // Attributes
        bool m_privateDSN;
        std::string m_protocol;
        std::string m_host;
        std::string m_publicKey;
        std::string m_secretKey;
        std::string m_projectID;

        std::string m_sentryVersion;
        std::string m_version;

        // Enums
        enum Exception { SEPARATOR_NOT_FOUND = 0 };
};

#endif //PINGCOUNT_DSN_H
