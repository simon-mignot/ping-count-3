#!/bin/bash

pistache_version=${1:-latest}
restcpp_version=${2:-latest}

apt-get update
apt-get install -y libcurl4-openssl-dev build-essential libtool cmake autoconf git

### Création d'un repertoire temporaire
mkdir pingcount-install-tmp
cd pingcount-install-tmp    # BASE_DIR/pingcount-install-tmp/

#### Bibliothèques
### Pistache
## Récuperation des sources
git clone https://github.com/oktal/pistache.git
cd pistache                 # BASE_DIR/pingcount-install-tmp/pistache/
if [ "$pistache_version" != "latest" ]
then
    git reset --hard $pistache_version
fi
git submodule update --init

## Compilation
mkdir build
cd build                    # BASE_DIR/pingcount-install-tmp/pistache/build/
cmake -G "Unix Makefiles" -DCMAKE_BUILD_TYPE=Release ..
make

## Installation sur le système
make install

cd ../..                    # BASE_DIR/pingcount-install-tmp/
### restclient-cpp
## Récupération des sources
git clone https://github.com/mrtazz/restclient-cpp.git
cd restclient-cpp           # BASE_DIR/pingcount-install-tmp/restclient-cpp/
if [ "$restcpp_version" != "latest" ]
then
    git reset --hard $restcpp_version
fi

## Configuration
./autogen.sh
./autogen.sh # need to be launched twice in some case
./configure

## Compilation et installation
make install

cd ../..                    # BASE_DIR/
### Suppression du répertoire temporaire
rm -rf ./pingcount-install-tmp/

#### Mise à jour de la liste des bibliothèques
ldconfig
