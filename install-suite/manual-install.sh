#!/bin/bash

### Dépendances
sudo apt-get update
sudo apt-get install -y libcurl4-openssl-dev build-essential libtool cmake autoconf git

### Création d'un repertoire temporaire
cd ..
mkdir pingcount-install-tmp
cd pingcount-install-tmp    # BASE_DIR/pingcount-install-tmp/

#### Bibliothèques
### Pistache
## Récuperation des sources
git clone https://github.com/oktal/pistache.git
cd pistache                 # BASE_DIR/pingcount-install-tmp/pistache/
git submodule update --init

## Compilation
mkdir build
cd build                    # BASE_DIR/pingcount-install-tmp/pistache/build/
cmake -G "Unix Makefiles" -DCMAKE_BUILD_TYPE=Release ..
make

## Installation sur le système
sudo make install

cd ../..                    # BASE_DIR/pingcount-install-tmp/
### restclient-cpp
## Récupération des sources
git clone https://github.com/mrtazz/restclient-cpp.git
cd restclient-cpp           # BASE_DIR/pingcount-install-tmp/restclient-cpp/

## Configuration
./autogen.sh
./autogen.sh
./configure

## Compilation et installation
sudo make install

cd ../..                    # BASE_DIR/
### Suppression du répertoire temporaire
sudo rm -rf ./pingcount-install-tmp/

#### Mise à jour de la liste des bibliothèques
sudo ldconfig


#### Installation de PingCount
### Génération des fichiers de compilation
cd ping-count-3/
mkdir cmake-build-release
cd cmake-build-release      # BASE_DIR/ping-count-3/cmake-build-release/
cmake -DCMAKE_BUILD_TYPE=Release -G "Unix Makefiles" ../

### Compilation
make

### Installation
mv PingCount ../../PingCount

